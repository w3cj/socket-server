module.exports = function(io) {
  io.on('connection', function (socket) {
    console.log('Client connected:', socket.id);

    socket.on('disconnect', function (data) {
      console.log('Client disconnected:', socket.id);
    });

    socket.on('hello', function (data) {
      console.log(data);
      socket.emit('hello', data);
    });
  });
}
